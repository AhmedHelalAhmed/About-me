# My Projects

## JavaScript and jQuery 
[Game-Of-Life](https://github.com/AhmedHelalAhmed/Game-Of-Life)

## php native
[ecommerce mobile](https://github.com/AhmedHelalAhmed/PHP-Project)

## XML and native php
[XML-Project](https://github.com/AhmedHelalAhmed/XML-Project)

## Laravel ( php )
[Hotel system](https://github.com/AhmedHelalAhmed/Laravel-Project)

## python - django 
[Blog](https://github.com/AhmedHelalAhmed/python_project)

## Bash
[DBMS](https://github.com/AhmedHelalAhmed/Bash-Project)

## Ruby on rails
[MOOC](https://github.com/AhmedHelalAhmed/Ruby-On-Rails-Project)

## Laravel
[ITI_Graduation_Project](https://github.com/AhmedHelalAhmed/ITI_Graduation_Project)

# My Accounts

## LinkedIn
[ahmedhelalahmed](https://www.linkedin.com/in/ahmedhelalahmed)

## Twitter
[for_ahmed_helal](https://twitter.com/for_ahmed_helal)

## Facebook
[A.Helal.A.A](https://www.facebook.com/A.Helal.A.A)
<br/>
[for.ahmed.helal](https://www.facebook.com/for.ahmed.helal)

## Codepen
[AhmedHelalAhmed](https://codepen.io/AhmedHelalAhmed)

## Sololearn
[AhmedHelalAhmed](https://www.sololearn.com/Profile/2476537)

## Stackoverflow
[AhmedHelalAhmed](https://www.quora.com/profile/Ahmed-Helal-12)

## Quora
[AhmedHelalAhmed](https://stackoverflow.com/users/8844879/ahmed-helal-ahmed)

## Freecodecamp
[AhmedHelalAhmed](https://www.freecodecamp.org/ahmedhelalahmed)

## Waffle
[AhmedHelalAhmed](https://waffle.io/AhmedHelalAhmed)

# My Emails

## Gmail
[Ahmed.Helal.Ahmed.Ali](mailto:Ahmed.Helal.Ahmed.Ali@gmail.com)
<br/>
[Ahmed.Helal.online](mailto:Ahmed.Helal.online@gmail.com) 

# My CV
[Ahmed Helal Ahmed](Ahmed-Helal-Ahmed.pdf)



